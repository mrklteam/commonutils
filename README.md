# CommonUtils

[![CI Status](http://img.shields.io/travis/Karen Lusinyan/CommonUtils.svg?style=flat)](https://travis-ci.org/Karen Lusinyan/CommonUtils)
[![Version](https://img.shields.io/cocoapods/v/CommonUtils.svg?style=flat)](http://cocoapods.org/pods/CommonUtils)
[![License](https://img.shields.io/cocoapods/l/CommonUtils.svg?style=flat)](http://cocoapods.org/pods/CommonUtils)
[![Platform](https://img.shields.io/cocoapods/p/CommonUtils.svg?style=flat)](http://cocoapods.org/pods/CommonUtils)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

CommonUtils is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "CommonUtils"
```

## Author

Karen Lusinyan, karen.lusinyan.developerios@gmail.com

## License

CommonUtils is available under the MIT license. See the LICENSE file for more info.

RELEASE-INFO: pod trunk push CommonUtils.podspec