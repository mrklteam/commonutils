//  Created by Karen Lusinyan on 16/07/14.

#import "Canvas.h"

@interface CommonBookContentViewController : UIViewController

@property (readwrite, nonatomic, strong) UIImage *image;

+ (instancetype)instance;

@end
