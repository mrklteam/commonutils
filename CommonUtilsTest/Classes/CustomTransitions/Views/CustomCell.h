//  Created by Karen Lusinyan on 16/04/14.

#import "Canvas.h"

@interface CustomCell : UICollectionViewCell

@property (nonatomic, strong) CSAnimationView *imageViewCanvas;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *lblNome;
@property (nonatomic, strong) UILabel *lblDescr;
@property (nonatomic, strong) UILabel *lblPeriodo;

@end
